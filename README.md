# IOT-CERTIFICATE

## Compile From Source
### Debian/Ubuntu
These instructions are intended for Debian and Ubuntu Linux distributions. For other distributions please use your respective package manager to install the dependencies.
#### 1) Install the necessary libraries
```
sudo apt-get update
sudo apt-get install make gcc g++ uuid-dev libssl-dev check libsubunit-dev git -y
```

cmake version >=3.7 is required. Check the version of `cmake` in your apt repositories with `apt-cache policy cmake`. If your version of Debian/Ubuntu has an equal or higher version of cmake then install it using:
```
sudo apt-get install cmake -y
```
If your cmake version is lower than 3.7 then see section [Compile cmake from source](#compile-cmake-from-source).

##### 2) Clone the repository and compile the sources
Download the latest version of the iot-certificate:
```
git clone https://gitlab.com/posejdon/iot-certificate.git
cd iot-certificate
```

Create build directory to contain all generated files:
```
mkdir build
cd build
```

Generate Makefile
```
cmake ..
```

Compile!
```
make
```

To run the tests:
``` 
make test
```

The output of the tests should look something like this:
```
julek@debian:~/iot-certificate/build$ make test
Running tests...
Test project /home/julek/projects/magisterka/build
    Start 1: BasicTests
1/2 Test #1: BasicTests .......................   Passed    0.00 sec
    Start 2: IOTTests
2/2 Test #2: IOTTests .........................   Passed    1.15 sec

100% tests passed, 0 tests failed out of 2

Total Test time (real) =   1.16 sec
```


# Compile cmake from source
To install for example version 3.13.0-rc2 from source do the following:
```
wget https://cmake.org/files/v3.13/cmake-3.13.0-rc2.tar.gz
tar -xzf cmake-3.13.0-rc2.tar.gz
cd cmake-3.13.0-rc2
./bootstrap
make
sudo make install
```



