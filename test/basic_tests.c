#include <check.h>
#include <stdlib.h>

START_TEST(true_test)
{
  ck_assert_int_eq(5, 5);
}
END_TEST


Suite * basic_suite(void)
{
    Suite *s;
    TCase *tc_main;

    s = suite_create("Basic");

    tc_main = tcase_create("main");

    tcase_add_test(tc_main, true_test);
    suite_add_tcase(s, tc_main);

    return s;
}

int main(void)
{
    int number_failed;
    Suite *s;
    SRunner *sr;

    s = basic_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
