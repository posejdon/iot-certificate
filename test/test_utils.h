#ifndef TEST_UTILS
#define TEST_UTILS

#include <stdio.h>
#include <iot_certificate.h>

#define CUSTOM_START_TEST(__testname)\
START_TEST(__testname)\
    printf("\n########## Starting test %s ##########\n", #__testname);


#define CUSTOM_END_TEST\
    printf("\n########## Ending test ##########\n");\
END_TEST

void check_certificate(IotCertificate* iot_cert, uint8_t* src);
    
#endif /* TEST_UTILS */
