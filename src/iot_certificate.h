#ifndef IOT_CERTIFICATE
#define IOT_CERTIFICATE
#include <stdint.h>

typedef struct {
    // key is the DER ASN.1 binary encoding of the public/private key. Its
    // structure is explained in https://stackoverflow.com/a/29707204
    // The type of key may be discovered by using the Object Identifier
    // of the DER structure.
    // Object Identifier values may be found in Appendix A of rfc5480.
    uint16_t key_len;
    uint8_t* key; // Certificate Key
} IotKey;

typedef struct {
    IotKey pubkey;
    uint16_t signature_len;
    uint8_t* signature;
} IotSignature;

typedef enum {
    LIST, // Only the first Identifier is allowed to be a list. After that it is treated as an error.
          // The list is really just a sequence of CertificateOwnerData structs.
    STRING,
    JSON, // JSON is basically a string but with a two byte length field
} __attribute__((packed)) IdentifierType;

typedef struct {
    uint16_t len; // 65 KB should be more than enough to convey all identity information
    uint8_t* arr;
} ListIdentifier;

typedef struct {
    uint8_t len;
    uint8_t* str;
} StringIdentifier;

typedef struct {
    uint16_t len;
    uint8_t* json;
} JsonIdentifier;

typedef struct {
    IdentifierType type;
    uint8_t* data;
} CertificateOwnerData;

typedef enum {
    EXPIRATION_DATE,
    ALLOWED_NETWORK_ipv4,
} __attribute__((packed)) OptionType;

typedef struct {
    uint32_t time;
} ExpirationDateOption;

typedef struct {
    uint8_t network[4];
    uint8_t mask[4];
} AllowedNetworkIpv4Option;

typedef struct {
    OptionType type;
    uint8_t* data;
} IotCertOption;

typedef struct {
    uint16_t len;
    uint8_t* data;
} IotCertOptionsList;

typedef struct {
    uint8_t version_number;
    uint8_t uuid[16];
    IotKey cert_key;
    CertificateOwnerData cod;
    IotCertOptionsList options;
    uint8_t signature_count;
    IotSignature* signatures;
} IotCertificate;

#endif /* IOT_CERTIFICATE */

