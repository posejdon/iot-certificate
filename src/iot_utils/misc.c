#include "misc.h"

void printf_n_bytes(const uint8_t* buffer, size_t n) {
    for (int i=0; i<n; i++) {
        printf("%02x ", buffer[i]);
    }
}

int copy_and_ignore(char** dest, const char* src, const char ignore_char, int len) {
    int dest_len = 0, i, j;
    
    for (i=0; i<len; i++) {
        if (src[i] != ignore_char) {
            dest_len++;
        }
    }
    
    char* _dest = (char*) malloc(dest_len);
    
    for (i=0, j=0; j<dest_len; i++) {
        if (src[i] != ignore_char) {
            _dest[j++] = src[i];
        }
    }
    
    *dest = _dest;
    return dest_len;
}
