/*
 * Generators that will properly initialize an
 * IOT certificate. Functionality for IOT certificates
 * was divided between the generator and the utility
 * functions since the generator uses libraries that
 * would not be available in an embedded device. If
 * you want to try to get the uuid header onto an
 * embedded device then be my guest, I'm not doing it.
 * Additionally embedded devices are not probably
 * going to generate their own certificates, it will
 * be done beforehand on a computer or server and the
 * certificate will be installed directly on the device.
 */

#ifndef IOT_CERTIFICATE_GENERATOR
#define IOT_CERTIFICATE_GENERATOR

#include <iot_certificate.h>
#include <iot_utils/misc.h>
#include <iot_utils/iot_openssl.h>
#include <iot_utils/narg.h>

#include <uuid/uuid.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/ossl_typ.h>
#include <openssl/rand.h>

int generate_certificate(
    IotCertificate* iot_cert,
    IdentifierType IdType,
    void* Id,
    IotCertOptionsList* options,
    EVP_PKEY* key_pair);
void free_certificate(IotCertificate *iot_cert);
int serialize_certificate(IotCertificate *iot_cert, uint8_t** dest);
int serialize_certificate_without_signatures(IotCertificate *iot_cert, uint8_t** dest);
int sign_certificate(EVP_PKEY* pkey, IotCertificate* iot_cert);
int add_signature(IotCertificate* iot_cert, EVP_PKEY* pkey, uint8_t* signature, size_t sign_len, int copy);

// IotCertOptionsList helper functions
// This macro forces the preprocessor to count the number of arguments it was passed.
#define MAKE_OPTIONS_LIST(opt_list, ...) __make_options_list(opt_list, PP_NARG(__VA_ARGS__), __VA_ARGS__)
int __make_options_list(IotCertOptionsList* opt_list, size_t opt_count, ...);
int get_option_len(OptionType type);
int serialize_option(IotCertOption* opt_dest, OptionType type, void* opt_src);
// End IotCertOptionsList


/**
* @brief Populates an IotCertificate struct. The in_place parameter
* is important since it determines whether or not the struct will
* have to be freed. If in_place is 1 then it is assumed that enough
* IotSignature have been allocated in iot_cert->signatures. The
* proper iot_cert->signatures size may be declared using 
* 
* @param iot_cert: The struct to be populated.
* @param src; The buffer to be parsed.
* @param in_place: Should pointers be directed at the 
* src buffer or should new memory from the heap be allocated.
* @return int 1 on success and <=0 on failure
*/
int parse_certificate(IotCertificate* iot_cert, uint8_t* src, int in_place);

/**
* @brief This macro allows for an IotCertificate struct to be present
* only in the stack. This also allows for in-place parsing of the
* certificate as it means that parsing doesn't have to allocate
* memory from the heap. This macro uses a Variable Length Array to
* place the signature array on the stack.
* 
* @param iot_cert: The name of the newly declared IotCertificate
* struct.
* @param signature_count: The number of IotSignature structs to
* allocate.
* 
*/
#define IOT_VLA_SIG(iot_cert, signature_count) \
IotSignature __##iot_cert##_signature_array[signature_count];\
IotCertificate iot_cert = { .signatures = __##iot_cert##_signature_array }

int get_signature_count(uint8_t* buffer);

// CertificateOwnerData helper functions
void parse_cod_data(IdentifierType IdType, void* data, void** dest); // This function creates and populates a COD struct from data returns the pointer to the new struct in dest
int cod_struct_to_bytes_malloc(IdentifierType IdType, void* Id, uint8_t** dest);
int cod_struct_to_bytes(IdentifierType IdType, void* Id, uint8_t* dest);
int get_cod_len(CertificateOwnerData* cod);
int get_identifier_len(IdentifierType IdType, void* Id);
// End CertificateOwnerData

#endif /* IOT_CERTIFICATE_GENERATOR */
