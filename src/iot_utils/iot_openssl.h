#ifndef IOT_OPENSSL_UTILS
#define IOT_OPENSSL_UTILS

#include <string.h>
#include <uuid/uuid.h>
#include <iot_utils/base64.h>
#include <iot_utils/misc.h>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/ossl_typ.h>
#include <openssl/rand.h>

typedef enum {
    PUBLIC_KEY,
    PRIVATE_KEY
} __attribute__((packed)) KeyType;

// PEM headers and tails declared without a null-terminator
static const char PEM_PUBLIC_HEAD[]  = {
    '-','-','-','-','-',
    'B','E','G','I','N',' ','P','U','B','L','I','C',' ','K','E','Y',
    '-','-','-','-','-','\n'
};
static const char PEM_PUBLIC_TAIL[]  = {
    '-','-','-','-','-',
    'E','N','D',' ','P','U','B','L','I','C',' ','K','E','Y',
    '-','-','-','-','-','\n'
};
static const char PEM_PRIVATE_HEAD[] = {
    '-','-','-','-','-',
    'B','E','G','I','N',' ','P','R','I','V','A','T','E',' ','K','E','Y',
    '-','-','-','-','-','\n'
};
static const char PEM_PRIVATE_TAIL[] = {
    '-','-','-','-','-',
    'E','N','D',' ','P','R','I','V','A','T','E',' ','K','E','Y',
    '-','-','-','-','-','\n'
};

#define PEM_LINE_LEN 64

int gen_key(EVP_PKEY** key, EVP_PKEY_CTX* context);
int init_rsa_context(EVP_PKEY_CTX** context, int key_len);
int get_key_from_EVP_PKEY(uint8_t** d_key, EVP_PKEY* s_key, uint16_t* key_len, KeyType key_type);
int PEM_to_binary(uint8_t** dest, char* src, int src_len);
int generate_rsa_keypair(EVP_PKEY** key_pair, int key_len);
int binary_to_EVP_PKEY(EVP_PKEY** key, uint8_t* asn1_key, int len, KeyType key_type);
int binary_to_PEM(uint8_t** PEM_key, uint8_t* asn1_key, int len, KeyType key_type);

#endif /* IOT_OPENSSL_UTILS */
