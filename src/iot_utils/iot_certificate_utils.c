#include "iot_certificate_utils.h"

int generate_certificate(
        IotCertificate* iot_cert,
        IdentifierType IdType,
        void* Id,
        IotCertOptionsList* options,
        EVP_PKEY* key_pair
    ) {
    IotCertificate _iot_cert;
    uuid_t random_uuid;
    int ret = 1;

    uuid_generate_random(random_uuid);

    _iot_cert.version_number = 1;
    memcpy(_iot_cert.uuid, random_uuid, sizeof(_iot_cert.uuid));

    printf("type size: %lu value: %d\n", sizeof(EVP_PKEY_EC), EVP_PKEY_EC);
    
    
    if ((ret = get_key_from_EVP_PKEY(&(_iot_cert.cert_key.key), key_pair, &(_iot_cert.cert_key.key_len), PUBLIC_KEY)) <= 0) {
        printf("get_key_from_EVP_PKEY failed\n");
        goto get_key_from_EVP_PKEY_err;
    }
    
    _iot_cert.cod.type = IdType;
    if ((ret = cod_struct_to_bytes_malloc(IdType, Id, &(_iot_cert.cod.data))) <= 0) {
        printf("cod_data_to_bytes failed\n");
        goto cod_struct_to_bytes_malloc_err;
    }
    
    if (options) {
        _iot_cert.options.len = options->len;
        _iot_cert.options.data = malloc(options->len);
        memcpy(_iot_cert.options.data, options->data, options->len);
    } else {
        _iot_cert.options.len = 0;
        _iot_cert.options.data = NULL;
    }
    
    // No errors occured
    _iot_cert.signature_count = 0;
    _iot_cert.signatures = NULL;
    
    memcpy(iot_cert, &_iot_cert, sizeof(IotCertificate));

    printf("Properly generated RSA key!\n");
    
    ret = 1;
    
    // Error handling and cleanup
cod_struct_to_bytes_malloc_err:
get_key_from_EVP_PKEY_err:
    
    return ret;
}

int serialize_certificate(IotCertificate *iot_cert, uint8_t** dest) {
    uint8_t* buffer;
    uint8_t* _buffer;
    int cert_len;
    int signature_len = 0;
    int ret_code = 0;
    
    if ((cert_len = serialize_certificate_without_signatures(iot_cert, &buffer)) <= 0) {
        ret_code = 0;
        goto serialize_certificate_without_signatures_err;
    }
    
    signature_len += sizeof(iot_cert->signature_count);
    IotSignature* signature;
    for (int i = 0; i<iot_cert->signature_count; i++) {
        signature = &iot_cert->signatures[i];
        signature_len += sizeof(signature->pubkey.key_len) + signature->pubkey.key_len + sizeof(signature->signature_len) + signature->signature_len;
    }
    
    
    if (!(_buffer = realloc(buffer, cert_len + signature_len))) {
        ret_code = -1;
        goto realloc_err;
    }
    buffer = _buffer;
    
    uint8_t* iter = buffer + cert_len;
    *(typeof(iot_cert->signature_count)*) iter = iot_cert->signature_count;
    iter += sizeof(iot_cert->signature_count);
    
    for (int i = 0; i<iot_cert->signature_count; i++) {
        signature = &iot_cert->signatures[i];
        
        // Copy signature key length to memory
        *(typeof(signature->pubkey.key_len)*) iter = signature->pubkey.key_len;
        memcpy(iter + sizeof(signature->pubkey.key_len), signature->pubkey.key, signature->pubkey.key_len);
        // Move to hash portion
        iter += sizeof(signature->pubkey.key_len) + signature->pubkey.key_len;
        
        *(typeof(signature->signature_len)*) iter = signature->signature_len;
        memcpy(iter + sizeof(signature->signature_len), signature->signature, signature->signature_len);
        iter += sizeof(signature->signature_len) + signature->signature_len;
    }
    
    // No errors occured
    *dest = buffer;
    ret_code = cert_len + signature_len;
    
    // Error handling and cleanup
realloc_err:
    if (ret_code <= 0) {
        free(buffer);
    }
serialize_certificate_without_signatures_err:
    return ret_code;
}

int serialize_certificate_without_signatures(IotCertificate *iot_cert, uint8_t** dest) {
    int ret_code = 0;
    int cert_len = 0;
    cert_len += sizeof(iot_cert->version_number) + sizeof(iot_cert->uuid);
    cert_len += sizeof(iot_cert->cert_key.key_len) + iot_cert->cert_key.key_len;
    int cod_len;
    if ((cod_len = get_cod_len(&(iot_cert->cod)))) {
        cert_len += cod_len;
    } else {
        printf("Got invalid COD length\n");
        goto get_cod_len_err;
    }
    cert_len += sizeof(iot_cert->options.len) + iot_cert->options.len;
    
    uint8_t* buffer = (uint8_t*) malloc(cert_len);
    uint8_t* iter = buffer;
    int copy_size;
    
    // COPY version_number + uuid
    copy_size = sizeof(iot_cert->version_number) + sizeof(iot_cert->uuid);
    memcpy(iter, iot_cert, copy_size);
    iter += copy_size;
    
    // COPY cert_key
    copy_size = sizeof(iot_cert->cert_key.key_len) + iot_cert->cert_key.key_len;
    *(uint16_t*) iter = iot_cert->cert_key.key_len;
    memcpy(iter + sizeof(iot_cert->cert_key.key_len), iot_cert->cert_key.key, iot_cert->cert_key.key_len);
    iter += copy_size;
    
    // COPY cod
    *(IdentifierType*) iter = iot_cert->cod.type;
    void* cod_struct;
    parse_cod_data(iot_cert->cod.type, iot_cert->cod.data, &cod_struct);
    if (cod_struct == NULL) {
        printf("parse_cod_data failed\n");
        ret_code = -1;
        goto parse_cod_data_err;
    }
    if (cod_struct_to_bytes(iot_cert->cod.type, cod_struct, iter + sizeof(iot_cert->cod.type)) <= 0) {
        printf("cod_data_to_bytes failed\n");
        ret_code = -2;
        goto cod_struct_to_bytes_err;
    }
    iter += cod_len;
    
    // COPY options
    *(typeof(iot_cert->options.len)*) iter = iot_cert->options.len;
    iter += sizeof(iot_cert->options.len);
    memcpy(iter, iot_cert->options.data, iot_cert->options.len);
    iter += iot_cert->options.len;
    
    // No errors occured
    ret_code = cert_len;
    *dest = buffer;
    
    // Error handling and cleanup
cod_struct_to_bytes_err:
    free(cod_struct);
parse_cod_data_err:
    if (ret_code <= 0) {
        free(buffer);
    }
get_cod_len_err:
    return ret_code;
}

void parse_cod_data(IdentifierType IdType, void* data, void** dest) {
    switch (IdType) {
        case LIST: {
            ListIdentifier* list_id = (ListIdentifier*) malloc(sizeof(ListIdentifier));
            list_id->len = *(typeof(list_id->len)*) data;
            list_id->arr = data + sizeof(list_id->len);
            *dest = list_id;
            break;
        }
        case STRING: {
            StringIdentifier* str_id = (StringIdentifier*) malloc(sizeof(StringIdentifier));
            str_id->len = *(typeof(str_id->len)*) data;
            str_id->str = data + sizeof(str_id->len);
            *dest = str_id;
            break;
        }
        case JSON: {
            JsonIdentifier* json_id = (JsonIdentifier*) malloc(sizeof(JsonIdentifier));
            json_id->len = *(typeof(json_id->len)*) data;
            json_id->json = data + sizeof(json_id->len);
            *dest = json_id;
            break;
        }
        default:
            *dest = NULL;
    }
}

int cod_struct_to_bytes_malloc(IdentifierType IdType, void* Id, uint8_t** dest) {
    uint8_t* buffer;
    int id_len;
    
    if ((id_len = get_identifier_len(IdType, Id)) <= 0) {
        return 0;
    }
    
    buffer = (uint8_t*) malloc(id_len);
    
    if (cod_struct_to_bytes(IdType, Id, buffer) <= 0) {
        free(buffer);
        return -1;
    }
    
    *dest = buffer;
    return id_len;
}


int cod_struct_to_bytes(IdentifierType IdType, void* Id, uint8_t* dest) {
    int cod_data_len;
    
    switch (IdType) {
        case LIST: {
            ListIdentifier* list_id = (ListIdentifier*) Id;
            cod_data_len = get_identifier_len(LIST, Id);
            *(typeof(list_id->len)*)dest = list_id->len;
            memcpy(dest + sizeof(list_id->len), list_id->arr, list_id->len);
            break;
        }
        case STRING: {
            StringIdentifier* str_id = (StringIdentifier*) Id;
            cod_data_len = get_identifier_len(STRING, Id);
            *(typeof(str_id->len)*)dest = str_id->len;
            memcpy(dest + sizeof(str_id->len), str_id->str, str_id->len);
            break;
        }
        case JSON: {
            JsonIdentifier* json_id = (JsonIdentifier*) Id;
            cod_data_len = get_identifier_len(JSON, Id);
            *(typeof(json_id->len)*)dest = json_id->len;
            memcpy(dest + sizeof(json_id->len), json_id->json, json_id->len);
            break;
        }
        default:
            return 0;
    }
    
    return cod_data_len;
}

int get_cod_len(CertificateOwnerData* cod) {
    return sizeof(cod->type) + get_identifier_len(cod->type, cod->data);
}

int get_identifier_len(IdentifierType IdType, void* Id) {
    int id_len;
    switch (IdType) {
        case LIST: {
            ListIdentifier* list_id = (ListIdentifier*) Id;
            id_len = sizeof(list_id->len) + list_id->len;
            break;
        }
        case STRING: {
            StringIdentifier* str_id = (StringIdentifier*) Id;
            id_len = sizeof(str_id->len) + str_id->len;
            break;
        }
        case JSON: {
            JsonIdentifier* json_id = (JsonIdentifier*) Id;
            id_len = sizeof(json_id->len) + json_id->len;
            break;
        }
        default:
            return 0;
    }
    return id_len;
}

int sign_certificate(EVP_PKEY* pkey, IotCertificate* iot_cert) {
    // Usefull resource: https://wiki.openssl.org/index.php/EVP_Signing_and_Verifying
    EVP_MD_CTX *mdctx = NULL;
    int ret_code = 0, cert_len;
    uint8_t* cert_serialized;
    size_t sign_len = 0;
    
    if ((cert_len = serialize_certificate_without_signatures(iot_cert, &cert_serialized)) <= 0) {
        printf("serialize_certificate_without_signatures failed\n");
        ret_code = 0;
        goto serialize_certificate_without_signatures_err;
    }
    
    uint8_t* signature = NULL;
    
    /* Create the Message Digest Context */
    if (!(mdctx = EVP_MD_CTX_create())) {
        ret_code = -1;
        goto EVP_MD_CTX_create_err;
    }
    
    /* Initialise the DigestSign operation - SHA-256 has been selected as the message digest function in this example */
    if(1 != EVP_DigestSignInit(mdctx, NULL, EVP_sha256(), NULL, pkey)) {
        ret_code = -2;
        goto EVP_DigestSignInit_err;
    }
    
    /* Call update with the message */
    if(1 != EVP_DigestSignUpdate(mdctx, cert_serialized, cert_len)) {
        ret_code = -3;
        goto EVP_DigestSignUpdate_err;
    }
    
    /* Finalise the DigestSign operation */
    /* First call EVP_DigestSignFinal with a NULL sig parameter to obtain the length of the
    * signature. Length is returned in slen */
    if(1 != EVP_DigestSignFinal(mdctx, NULL, &sign_len)) {
        ret_code = -4;
        goto EVP_DigestSignFinal1_err;
    }
    /* Allocate memory for the signature based on size in slen */
    if (!(signature = (uint8_t*) malloc(sign_len))) {
        ret_code = -5;
        goto signature_malloc_err;
    }
    /* Obtain the signature */
    if (1 != EVP_DigestSignFinal(mdctx, signature, &sign_len)) {
        ret_code = -6;
        goto EVP_DigestSignFinal2_err;
    }
    
    if (add_signature(iot_cert, pkey, signature, sign_len, 0) <= 0) {
        ret_code = -7;
        goto add_signature_err;
    }
    
    // No errors occured
    ret_code = 1;
    
    // Error handling and cleanup
add_signature_err:
EVP_DigestSignFinal2_err:
    if (ret_code <= 0) {
        free(signature);
    }
signature_malloc_err:
EVP_DigestSignFinal1_err:
EVP_DigestSignUpdate_err:
EVP_DigestSignInit_err:
    EVP_MD_CTX_destroy(mdctx);
EVP_MD_CTX_create_err:
    free(cert_serialized);
serialize_certificate_without_signatures_err:
    
    return ret_code;
}

int add_signature(IotCertificate* iot_cert, EVP_PKEY* pkey,  uint8_t* signature, size_t sign_len, int copy)
{
    int ret_code = 0;
    
    // Use a temp var for the new array if for some reason realloc fails the old pointer is still valid
    IotSignature* new_signature_arr = (IotSignature*) realloc(iot_cert->signatures, (iot_cert->signature_count + 1) * sizeof(IotSignature));
    
    if (new_signature_arr == NULL) {
        printf("realloc failed");
        ret_code = 0;
        goto realloc_err;
    }
    
    uint8_t* pubkey;
    uint16_t key_len;
    
    if (get_key_from_EVP_PKEY(&pubkey, pkey, &key_len, PUBLIC_KEY) <= 0) {
        printf("get_key_from_EVP_PKEY failed");
        ret_code = -1;
        goto get_key_from_EVP_PKEY_err;
    }
    
    IotSignature* new_signature = &new_signature_arr[iot_cert->signature_count];
    new_signature->signature_len = sign_len;
    if (copy) {
        new_signature->signature = (uint8_t*) malloc(sign_len);
        memcpy(new_signature->signature, signature, sign_len);
    } else {
        new_signature->signature = signature;
    }
    new_signature->pubkey.key_len = key_len;
    new_signature->pubkey.key = pubkey;
    
    iot_cert->signature_count++;
    iot_cert->signatures = new_signature_arr;
    
    return 1;
    
get_key_from_EVP_PKEY_err:
    free(new_signature_arr);
realloc_err:
    return ret_code;
}

int parse_certificate(IotCertificate* iot_cert, uint8_t* src, int in_place) {
    // TODO Implement error checking
    if ((*(typeof(iot_cert->version_number)*) src) != 1) {
        printf("ERROR parsing certificates is only implemented for version 1\n");
        return 0;
    }
    
    uint8_t* iter = src;
    
    // version_number
    iot_cert->version_number = *(typeof(iot_cert->version_number)*) iter;
    iter += sizeof(iot_cert->version_number);
    
    // uuid
    memcpy(iot_cert->uuid, iter, sizeof(iot_cert->uuid));
    iter += sizeof(iot_cert->uuid);
    
    // cert_key
    iot_cert->cert_key.key_len = *(typeof(iot_cert->cert_key.key_len)*) iter;
    if (in_place) {
        iot_cert->cert_key.key = iter + sizeof(iot_cert->cert_key.key_len);
    } else {
        iot_cert->cert_key.key = (uint8_t*) malloc(iot_cert->cert_key.key_len);
        memcpy(iot_cert->cert_key.key, iter + sizeof(iot_cert->cert_key.key_len), iot_cert->cert_key.key_len);
    }
    iter += sizeof(iot_cert->cert_key.key_len) + iot_cert->cert_key.key_len;
    
    // cod
    iot_cert->cod.type = *(typeof(iot_cert->cod.type)*) iter;
    iter += sizeof(iot_cert->cod.type);
    int id_len = get_identifier_len(iot_cert->cod.type, iter);
    if (in_place) {
        iot_cert->cod.data = iter;
    } else {
        iot_cert->cod.data = (uint8_t*) malloc(id_len);
        memcpy(iot_cert->cod.data, iter, id_len);
    }
    iter += id_len;
    
    // options
    iot_cert->options.len = *(typeof(iot_cert->options.len)*) iter;
    iter += sizeof(iot_cert->options.len);
    if (iot_cert->options.len > 0) {
        if (in_place) {
            iot_cert->options.data = iter;
        } else {
            iot_cert->options.data = (uint8_t*) malloc(iot_cert->options.len);
            memcpy(iot_cert->options.data, iter, iot_cert->options.len);
        }
        iter += iot_cert->options.len;
    } else {
        iot_cert->options.data = NULL;
    }
    
    // signatures
    iot_cert->signature_count = *(typeof(iot_cert->signature_count)*) iter;
    iter += sizeof(iot_cert->signature_count);
    if (!in_place) {
        iot_cert->signatures = (IotSignature*) malloc(iot_cert->signature_count * sizeof(IotSignature));
    }
    
    IotSignature* signature;
    for (int i = 0; i<iot_cert->signature_count; i++) {
        signature = &iot_cert->signatures[i];
        
        // copy key
        signature->pubkey.key_len = *(typeof(signature->pubkey.key_len)*) iter;
        iter += sizeof(signature->pubkey.key_len);
        if (in_place) {
            signature->pubkey.key = iter;
        } else {
            signature->pubkey.key = (uint8_t*) malloc(signature->pubkey.key_len);
            memcpy(signature->pubkey.key, iter, signature->pubkey.key_len);
        }
        iter += signature->pubkey.key_len;
        
        // copy hash
        signature->signature_len = *(typeof(signature->signature_len)*) iter;
        iter += sizeof(signature->signature_len);
        if (in_place) {
            signature->signature = iter;
        } else {
            signature->signature = (uint8_t*) malloc(signature->signature_len);
            memcpy(signature->signature, iter, signature->signature_len);
        }
        iter += signature->signature_len;
    }
    
    return 1;
}

int get_signature_count(uint8_t* buffer) {
    uint8_t* iter = buffer;
    
    // version_number
    iter += member_size(IotCertificate, version_number);
    
    // uuid
    iter += member_size(IotCertificate, uuid);
    
    // cert_key
    iter += *(member_type(IotKey, key_len)*) iter;
    iter += member_size(IotKey, key_len);
    
    // cod
    CertificateOwnerData cod;
    cod.type = *(typeof(cod.type)*) iter;
    cod.data = iter + sizeof(cod.type);
    iter += get_cod_len(&cod);
    
    return *(member_type(IotCertificate, signature_count)*) iter;
}

void free_certificate(IotCertificate* iot_cert) {
    free(iot_cert->cert_key.key);
    free(iot_cert->cod.data);
    
    for (int i = 0; i<iot_cert->signature_count; i++) {
        free(iot_cert->signatures[i].signature);
        free(iot_cert->signatures[i].pubkey.key);
    }
    
    if (iot_cert->options.len) {
        free(iot_cert->options.data);
    }
    
    if (iot_cert->signatures != NULL) {
        free(iot_cert->signatures);
    }
}

int get_option_len(OptionType type) {
    switch (type) {
        case EXPIRATION_DATE:
            return member_size(ExpirationDateOption, time);
        case ALLOWED_NETWORK_ipv4:
            return member_size(AllowedNetworkIpv4Option, network) + member_size(AllowedNetworkIpv4Option, mask);
        default:
            return 0;
    }
}

int serialize_option(IotCertOption* opt_dest, OptionType type, void* opt_src) {
    int ret = 1;
    int opt_len;
    uint8_t* buffer;
    
    if ((opt_len = get_option_len(type)) <= 0) {
        return 0;
    }
    
    buffer = (uint8_t*) malloc(opt_len);
    
    switch (type) {
        case EXPIRATION_DATE:
            *(member_type(ExpirationDateOption, time)*) buffer = ((ExpirationDateOption*) opt_src)->time;
            break;
        case ALLOWED_NETWORK_ipv4:
            memcpy(buffer, ((AllowedNetworkIpv4Option*) opt_src)->network, member_size(AllowedNetworkIpv4Option, network));
            memcpy(buffer + member_size(AllowedNetworkIpv4Option, network), ((AllowedNetworkIpv4Option*) opt_src)->mask,
                   member_size(AllowedNetworkIpv4Option, mask));
            break;
        default:
            break;
    }
    
    if (ret > 0) {
        opt_dest->type = type;
        opt_dest->data = buffer;
    } else {
        free(buffer);
    }
    
    return ret;
}

int __make_options_list(IotCertOptionsList* opt_list, size_t opt_count, ...) {
    va_list args;
    va_start(args, opt_count);
    
    uint8_t* opt_data = NULL;
    size_t opt_data_len = 0;
    int ret = 1;
    int opt_len = 0;
    int offset = 0;
    
    for (int i = 0; i < opt_count; i++, offset += opt_len) {
        IotCertOption opt = va_arg(args, IotCertOption);
        
        if ((opt_len = sizeof(opt.type) + get_option_len(opt.type)) <= 0) {
            ret = 0;
            goto get_option_len_err;
        }
        opt_data_len += opt_len;
        
        opt_data = realloc(opt_data, opt_data_len);
        
        *(typeof(opt.type)*) opt_data = opt.type;
        memcpy(opt_data + offset + sizeof(opt.type), opt.data, opt_len - sizeof(opt.type));
    }
    
    opt_list->len = opt_data_len;
    opt_list->data = opt_data;
    
get_option_len_err:
    va_end(args);
    
    return ret;
}
