#ifndef MISC_UTILS
#define MISC_UTILS
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define typeof __typeof__

#define member_size(type, member) sizeof(((type *)0)->member)
#define member_type(type, member) typeof(((type *)0)->member)

void printf_n_bytes(const uint8_t* buffer, size_t n);
int copy_and_ignore(char** dest, const char* src, const char ignore_char, int len);

#endif /* MISC_UTILS */
 
