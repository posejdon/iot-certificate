#include "iot_openssl.h"

int gen_key(EVP_PKEY** pkey, EVP_PKEY_CTX* pkey_context) {
//     On *unix systems this may be used to force OpenSSL to use /dev/random as a PRNG.
//     if(RAND_load_file("/dev/random", 32) != 32) {
//         /* RAND_load_file failed */
//         printf("Error RAND_load_file\n");
//         return -2;
//     }

    /* Generate the key */
    if (EVP_PKEY_keygen(pkey_context, pkey) <= 0) {
        printf("Error EVP_PKEY_keygen\n");
        return -1;
    }

    return 1;
}

int init_rsa_context(EVP_PKEY_CTX** pkey_context, int key_len) {
    /* Create context for the key generation */
    if(!(*pkey_context = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, NULL))) {
        printf("Error EVP_PKEY_CTX_new_id\n");
        return 0;
    }

    if(EVP_PKEY_keygen_init(*pkey_context) <= 0) {
        printf("Error in EVP_PKEY_keygen_init\n");
        return -1;
    }

    /* RSA keys set the key length during key generation rather than parameter generation! */
    if(EVP_PKEY_CTX_set_rsa_keygen_bits(*pkey_context, key_len) <= 0) {
        printf("Error EVP_PKEY_CTX_set_rsa_keygen_bits\n");
        return -2;
    }

    return 1;
}

int generate_rsa_keypair(EVP_PKEY** key_pair, int key_len) {
    EVP_PKEY_CTX* pkey_context;
    *key_pair = NULL;
    if (init_rsa_context(&pkey_context, key_len) <= 0) {
        printf("init_rsa_context failed\n");
        goto init_rsa_context_err;
    }

    if (gen_key(key_pair, pkey_context) <= 0) {
        printf("gen_key failed\n");
        goto gen_key_err;
    }
    
    EVP_PKEY_CTX_free(pkey_context);
    return 1;
    
    // Error handling
gen_key_err:
    EVP_PKEY_CTX_free(pkey_context);
init_rsa_context_err:
    return 0;
}

int PEM_to_binary(uint8_t** dest, char* src, int src_len) {
    char* src_begin = src;
    char* eof = src + src_len - 1;
    char* src_end = eof;
    
    if (*src != '-') {
        printf("First character of PEM should be '-'\n");
        return 0;
    }
    
    while (src_begin < eof && *(++src_begin) == '-') ; // skip first five dashes
    while (src_begin < eof && *(++src_begin) != '-') ; // skip PEM title
    while (src_begin < eof && *(++src_begin) == '-') ; // skip second five dashes
    
    while (src_end > src && *(--src_end) != '-') ; // skip garbage that may be at the end of the key
    while (src_end > src && *(--src_end) == '-') ; // skip last five dashes
    while (src_end > src && *(--src_end) != '-') ; // skip tail
    while (src_end > src && *(--src_end) == '-') ; // skip second last five dashes
    
    if (src_begin >= src_end) {
        printf("s_begin is larger than s_end so PEM_to_binary will fail\n");
        return -1;
    }
    
    char* newline_removed;
    
    int copied;
    if (!(copied = copy_and_ignore(&newline_removed,
                                   src_begin, 
                                   '\n',
                                   (src_end - src_begin) + 2 /* Add one to make the difference the length and add another to reserve space for a null-terminator */))) {
        printf("copy_and_ignore returned empty string\n");
        free(newline_removed);
        return -2;
    }
    newline_removed[copied-1] = '\0';
    
    char* decoded = (char*) malloc(Base64decode_len(newline_removed));
    int decoded_len= Base64decode(decoded, newline_removed);
    
    *dest = (uint8_t*) decoded;
    
    free(newline_removed);
    
    return decoded_len;
}

int get_key_from_EVP_PKEY(uint8_t** d_key, EVP_PKEY* s_key, uint16_t* key_len, KeyType key_type) {
    BIO* mem_bio = BIO_new(BIO_s_mem());
    int ret = 1;
    
    switch (key_type) {
        case PUBLIC_KEY:
            if ((ret = PEM_write_bio_PUBKEY(mem_bio, s_key)) <= 0) {
                printf("PEM_write_bio_PUBKEY failed\n");
                goto PEM_write_bio_err;
            }
            break;
        case PRIVATE_KEY:
            if ((ret = PEM_write_bio_PrivateKey(mem_bio, s_key, NULL, NULL, 0, 0, NULL)) <= 0) {
                printf("PEM_write_bio_PrivateKey failed\n");
                goto PEM_write_bio_err;
            }
            break;
        default:
            printf("key_type not correct. PUBLIC_KEY or PRIVATE_KEY.\n");
            ret = -3;
            goto wrong_key_type_err;
    }
    
    char* pem_key;
    long read_size = BIO_get_mem_data(mem_bio, NULL);
    if (read_size ^ (read_size & 0xFFFF)) {
        printf("BIO_get_mem_data returned size larger than 16 bits.\n");
        ret = -4;
        goto read_size_err;
    }
    pem_key = (char*) malloc(read_size);
    
    if ((ret = BIO_read(mem_bio, pem_key, read_size)) <= 0) {
        printf("BIO_read failed\n");
        goto BIO_read_err;
    }
    
    if ((*key_len = PEM_to_binary(d_key, pem_key, read_size)) <= 0) {
        printf("PEM_to_binary returned empty string.\n");
        ret = -5;
        goto PEM_to_binary_err;
    }
    
    ret = 1;

PEM_to_binary_err:
BIO_read_err:
    free(pem_key);
    
read_size_err:
PEM_write_bio_err:
wrong_key_type_err:
    BIO_free(mem_bio);
    
    return ret;
}

int binary_to_EVP_PKEY(EVP_PKEY** key, uint8_t* asn1_key, int len, KeyType key_type) {
    EVP_PKEY* parsed_key;
    int ret = 1;
    BIO* mem_bio = BIO_new(BIO_s_mem());
    uint8_t* PEM_key;
    int PEM_len;
    
    if ((PEM_len = binary_to_PEM(&PEM_key, asn1_key, len, key_type)) <= 0) {
        ret = -4;
        printf("binary_to_PEM failed\n");
        goto binary_to_PEM_err;
    }
    
    if ((ret = BIO_write(mem_bio, PEM_key, PEM_len)) <= 0) {
        printf("BIO_write failed\n");
        goto BIO_write_err;
    }
    
    switch (key_type) {
        case PUBLIC_KEY:
            if (!(parsed_key = PEM_read_bio_PUBKEY(mem_bio, NULL, NULL, NULL))) {
                printf("PEM_read_bio_PUBKEY failed\n");
                ret = -2;
                goto PEM_read_bio_err;
            }
            break;
        case PRIVATE_KEY:
            if (!(parsed_key = PEM_read_bio_PrivateKey(mem_bio, NULL, NULL, NULL))) {
                printf("PEM_read_bio_PrivateKey failed\n");
                ret = -3;
                goto PEM_read_bio_err;
            }
            break;
        default:
            printf("key_type not correct. PUBLIC_KEY or PRIVATE_KEY.\n");
            ret = -4;
            goto wrong_key_type_err;
    }
    
    *key = parsed_key;
    ret = 1;
    
PEM_read_bio_err:
wrong_key_type_err:
BIO_write_err:
    free(PEM_key);
binary_to_PEM_err:
    BIO_free(mem_bio);
    
    return ret;
}

int binary_to_PEM(uint8_t** PEM_key, uint8_t* asn1_key, int len, KeyType key_type) {
    int ret = 0;
    int PEM_len = 0;
    int base64_len;
    uint8_t* base64_asn1_key;
    uint8_t* _PEM_key;
    
    switch (key_type) {
        case PUBLIC_KEY:
            PEM_len += sizeof(PEM_PUBLIC_HEAD);
            PEM_len += sizeof(PEM_PUBLIC_TAIL);
            break;
        case PRIVATE_KEY:
            PEM_len += sizeof(PEM_PRIVATE_HEAD);
            PEM_len += sizeof(PEM_PRIVATE_TAIL);
            break;
        default:
            printf("key_type not correct. PUBLIC_KEY or PRIVATE_KEY.\n");
            ret = -2;
            goto wrong_key_type_err;
    }
    
    base64_len = Base64encode_len(len);
    PEM_len += base64_len;
    PEM_len += (base64_len / PEM_LINE_LEN) + 1; // newlines
    
    base64_asn1_key = (uint8_t*) malloc(base64_len);
    if (Base64encode((char*) base64_asn1_key, (char*) asn1_key, len) <= 0) {
        ret = -3;
        printf("Base64encode error.\n");
        goto Base64encode_err;
    }
    
    _PEM_key = (uint8_t*) malloc(PEM_len);
    
    uint8_t* iter = _PEM_key;
    
    switch (key_type) {
        case PUBLIC_KEY:
            memcpy(_PEM_key, PEM_PUBLIC_HEAD, sizeof(PEM_PUBLIC_HEAD));
            memcpy(_PEM_key + PEM_len - sizeof(PEM_PUBLIC_TAIL), PEM_PUBLIC_TAIL, sizeof(PEM_PUBLIC_TAIL));
            iter += sizeof(PEM_PUBLIC_HEAD);
            break;
        case PRIVATE_KEY:
            memcpy(_PEM_key, PEM_PRIVATE_HEAD, sizeof(PEM_PRIVATE_HEAD));
            memcpy(_PEM_key + PEM_len - sizeof(PEM_PRIVATE_TAIL), PEM_PRIVATE_TAIL, sizeof(PEM_PRIVATE_TAIL));
            iter += sizeof(PEM_PRIVATE_HEAD);
            break;
        default:
            printf("key_type not correct. PUBLIC_KEY or PRIVATE_KEY.\n");
            ret = -2;
            goto wrong_key_type_2_err;
    }
    
    uint8_t* b64_iter = base64_asn1_key;
    int b64_copied;
    for (uint8_t* b64_end = base64_asn1_key + base64_len;
         b64_iter < b64_end;
         iter += PEM_LINE_LEN + 1 /*with newline*/, b64_iter += PEM_LINE_LEN) {
        b64_copied = b64_iter + PEM_LINE_LEN < b64_end ? PEM_LINE_LEN : b64_end - b64_iter;
        memcpy(iter, b64_iter, b64_copied);
        iter[b64_copied] = '\n';
    }
    
    ret = PEM_len;
    *PEM_key = _PEM_key;
    
wrong_key_type_2_err:
    if (ret <= 0) {
        free(_PEM_key);
    }

Base64encode_err:
    free(base64_asn1_key);

wrong_key_type_err:

    return ret;
}
